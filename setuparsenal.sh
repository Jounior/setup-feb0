#!/bin/bash
################################################
#   This tools setup the feb0 hacking arsenal
#   make by C0de
################################################


## update the system
sudo apt update -y
sudo apt upgrade -y

## verify if the dependeces already was installed


##Create and move to Arsenal folder
mkdir ~/Arsenal
cd ~/Arsenal



## install dependencies
sudo apt install git
sudo apt install curl
curl -sSL https://get.rvm.io | bash -s -- --autolibs=install-packages
sudo .rvm/bin/rvm requirements
curl -sSL https://get.rvm.io | bash -s stable --ruby
rvm install "ruby-2.5.3"

echo -e "Installing beef\n"
##Install beef
wget -O - https://raw.githubusercontent.com/beefproject/beef/master/install | bash

echo -e "Installing metasploit\n"
##install metasploit
curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && \
  chmod 755 msfinstall && \
  ./msfinstall

echo -e "Installing nmap\n"


##intall nmap 
sudo apt install nmap
